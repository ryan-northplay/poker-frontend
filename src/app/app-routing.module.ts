import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {EnterComponent} from "./enter/enter.component";
import {MainComponent} from "./main/main.component";

const routes: Routes = [
  { path: '', pathMatch: 'full', component: EnterComponent },
  { path: 'main', component: MainComponent },
  { path: '**', component: EnterComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
