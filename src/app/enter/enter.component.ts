import { Component, OnInit } from '@angular/core';
import {GoogleAnalyticsService} from "../services/googleAnalytics.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-enter',
  templateUrl: './enter.component.html',
  styleUrls: ['./enter.component.scss']
})
export class EnterComponent implements OnInit {

  agree = true;

  showTACModal: boolean;

  constructor(private googleAnalyticsService: GoogleAnalyticsService, private router: Router) { }

  ngOnInit(): void {
  }

  closeTacModal() {
    this.showTACModal = false;
    this.googleAnalyticsService.addStatEvent('close_terms_and_conditions', '', '', 'click', 1);
  }

  goToMain() {
    this.googleAnalyticsService.addStatEvent('enter', '', '', 'click', 1);
    this.router.navigate(['main']);
  }

  openTACModal(e) {
    this.googleAnalyticsService.addStatEvent('open_terms_and_conditions', '', '', 'click', 1);
    this.showTACModal = true;
    e.stopPropagation();
  }

}
