import { Injectable } from "@angular/core";

declare let gtag: Function;

@Injectable()
export class GoogleAnalyticsService {

  public addStatEvent(
    eventName: string,
    eventCategory: string,
    eventAction: string,
    eventLabel: string = '',
    eventValue: number = 0 ){
    gtag('event', eventName, {
      eventCategory: eventCategory,
      eventLabel: eventLabel,
      eventAction: eventAction,
      eventValue: eventValue
    })
  }

}
