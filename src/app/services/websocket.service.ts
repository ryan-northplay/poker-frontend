import { Injectable } from "@angular/core";
import {environment} from "../../environments/environment";

import {WebSocketSubject} from "rxjs/internal/observable/dom/WebSocketSubject";
import {Subject} from "rxjs/internal/Subject";
import {Observable} from "rxjs/internal/Observable";

@Injectable()
export class WebsocketService {
  id = 0;

  socket$: WebSocketSubject<object>;

  private connectionStartManager = new Subject<void>();
  connectionStart: Observable<void>;

  connect() {
    this.connectionStart = this.connectionStartManager.asObservable();
    this.socket$ = this.getNewWebSocket();
  }

  private getNewWebSocket() {
    return new WebSocketSubject<object>({
      url: environment.url,
      openObserver: {
        next: () => {
          this.connectionStartManager.next();
        }
      }
    });
  }

  send(msg: string, data?: object) {
    const obj = { id: this.id, msg, data: data || {} };
    this.socket$.next(obj);
    this.id++;
  }

  close() {
    this.socket$.complete();
  }



}
