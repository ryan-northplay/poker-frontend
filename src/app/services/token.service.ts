import { Injectable } from "@angular/core";

@Injectable()
export class TokenService {

  private tokenKey = 'poker-token';

  getToken() {
    return localStorage.getItem(this.tokenKey);
  }

  setToken(value: string) {
    return localStorage.setItem(this.tokenKey, value);
  }

  removeToken() {
    localStorage.removeItem(this.tokenKey);
  }

}
