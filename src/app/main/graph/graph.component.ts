import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-graph',
  templateUrl: './graph.component.html',
  styleUrls: ['./graph.component.scss']
})
export class GraphComponent implements OnInit {

  @Input() stats: any;

  constructor() { }

  ngOnInit(): void {
  }

  getWorseHandsPercents() {
    const onePercent = this.stats.totalHands / 100;
    return this.stats.worseHands / onePercent;
  }

  getBetterHandsPercents() {
    const onePercent = this.stats.totalHands / 100;
    return this.stats.betterHands / onePercent;
  }

  getRounded(num: number) {
    return Math.round(num);
  }

  getSameHandsPercents() {
    const onePercent = this.stats.totalHands / 100;
    return this.stats.sameHands / onePercent;
  }

}
