import {Component, HostListener, OnInit} from '@angular/core';
import {WebsocketService} from "../services/websocket.service";
import {merge, retryWhen, switchMap, tap} from "rxjs/operators";
import {TokenService} from "../services/token.service";
import {timer} from "rxjs/internal/observable/timer";
import {Subject} from "rxjs/internal/Subject";
import {GoogleAnalyticsService} from "../services/googleAnalytics.service";

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {

  @HostListener('window:keyup', ['$event'])
  keyEvent(event: KeyboardEvent) {
    const key = event.key;
    if (key === 'Escape') {
      if (this.showModal) {
        this.showModal = false;
      }
      if (this.showReconnectModal) {
        this.showReconnectModal = false;
      }
      return;
    }
    const lowerKey = key.toLowerCase();
    if (this.isRankSelectable()) {
      const rank = this.ranks.find(r => {
        if (Array.isArray(r.keys)) {
          return r.keys.indexOf(lowerKey) > -1;
        } else {
          return r.keys === lowerKey;
        }
      });
      if (rank) {
        this.setRank(rank.caption);
      }
    }
    const shortSuits = this.suits.map(s => s.short);
    if (this.isSuitSelectable() && shortSuits.indexOf(lowerKey) > -1) {
      const suit = this.suits.find(s => s.short === lowerKey);
      if (suit) {
        this.setSuit(suit.name);
      }
    }
  }

  ranks = [
    { caption: '2', keys: '2' },
    { caption: '3', keys: '3' },
    { caption: '4', keys: '4' },
    { caption: '5', keys: '5' },
    { caption: '6', keys: '6' },
    { caption: '7', keys: '7' },
    { caption: '8', keys: '8' },
    { caption: '9', keys: '9' },
    { caption: '10', keys: ['1', 't'] },
    { caption: 'J', keys: 'j' },
    { caption: 'Q', keys: 'q' },
    { caption: 'K', keys: 'k' },
    { caption: 'A', keys: 'a' }
  ];

  suits = [
    { name: 'diamonds', short: 'd' },
    { name: 'hearts', short: 'h' },
    { name: 'spades', short: 's' },
    { name: 'clubs', short: 'c' }
  ];

  cards = [
    {
      name: 'hole',
      caption: 'Hole cards',
      set: [{rank: '', suit: ''}, {rank: '', suit: ''}]
    },
    {
      name: 'flop',
      caption: 'Flop',
      set: [{rank: '', suit: ''}, {rank: '', suit: ''}, {rank: '', suit: ''}],
      search: this.fetchFlopStats.bind(this)
    },
    {
      name: 'turn',
      caption: 'Turn',
      set: [{rank: '', suit: ''}],
      search: this.fetchTurnStats.bind(this)
    },
    {
      name: 'river',
      caption: 'River',
      set: [{rank: '', suit: ''}],
      search: this.fetchRiverStats.bind(this)
    }
  ];

  selectedSet = this.cards[0].name;

  selectedPosition = 0;

  selectRank = true;

  flopStats: any;

  turnStats: any;

  riverStats: any;

  modalStats: any;

  showModal = false;

  modalMode: string = '';

  modalType: string = '';

  reconnectIntervals = [1,2,4,8,15,30,60];

  reconnectTryNumber = 0;

  showReconnectModal = false;

  showTACModal = false;

  flopHand: { ways: number, handTypeString: '' };

  turnHand: { ways: number, handTypeString: '' };

  riverHand: { ways: number, handTypeString: '' };

  reconnecting: boolean;

  reconnectNow = new Subject();

  constructor(private websocketService: WebsocketService, private tokenService: TokenService, private googleAnalyticsService: GoogleAnalyticsService) {

    this.websocketService.connectionStart.subscribe(() => {
      const token = this.tokenService.getToken();
      if (token) {
        this.websocketService.send('setToken', { token });
      } else {
        this.websocketService.send('getToken');
      };

      this.showReconnectModal = false;
      this.reconnectTryNumber = 0;
    });

    this.websocketService.socket$
      .pipe(
        retryWhen(errors => errors.pipe(
          tap(() => {
            this.showReconnectModal = true;
            this.reconnecting = false;
          }),
          merge(this.reconnectNow.asObservable()),
          switchMap(() => timer(this.getReconnectTime()).pipe(tap(() => { this.reconnecting = true; }))),
          tap(() => this.reconnectTryNumber++)))
      )
      .subscribe((res: any) => {
        if (res.code !== 200) {
          console.log(`Request failed with code ${res.code}: ${res.info}`);
          if (res.msg === 'setToken') {
            this.tokenService.removeToken();
            this.websocketService.send('getToken');
          }
          return;
        }
        switch (res.msg) {
          case 'flopSearch':
            this.flopStats = res.data;
            this.flopHand = this.flopStats.betterHandsList[0];
            break;
          case 'turnSearch':
            this.turnStats = res.data;
            this.turnHand = this.turnStats.betterHandsList[0];
            break;
          case 'riverSearch':
            this.riverStats = res.data;
            this.riverHand = this.riverStats.betterHandsList[0];
            break;
          case 'getToken':
            const token = res.data.token;
            this.tokenService.setToken(token);
        }
      })
  }

  ngOnInit(): void {
  }

  closeTacModal() {
    this.showTACModal = false;
    this.googleAnalyticsService.addStatEvent('close_terms_and_conditions', '', '', '', 1);
  }

  getReconnectTime() {
    return (this.reconnectTryNumber > 6 ? this.reconnectIntervals[6] : this.reconnectIntervals[this.reconnectTryNumber]) * 1000;
  }

  getAllCards() {
    return this.cards.map(c => c.set).reduce((acc, val) => acc.concat(val));
  }

  getClassName(str: string) {
    return str.toLowerCase().split(' ').join('-');
  }

  getHandClasses(str: string, handValue) {
    const typeClass = this.getClassName(str);
    const activeClass = str === handValue.handTypeString ? 'active' : '';
    return `${typeClass} ${activeClass}`;
  }

  getCardClass(rank: string, suit: string) {
    return `${suit}-${rank}`;
  }

  getRankClass(rank: string) {
    return `rank-${rank}`;
  }

  getSuitClass(suit: string) {
    return `suit-${suit}`;
  }

  getStringifiedCards(set: {rank: string, suit: string}[]) {
    return set.map(s => {
      const rank = s.rank === '10' ? 't' : s.rank.toLowerCase();
      return `${rank}${s.suit[0]}`;
    }).join('');
  }

  checkStats() {
    const set = this.cards.find(c => c.name === this.selectedSet);
    if (!set) return;
    const setIndex = this.cards.indexOf(set);
    const setsToRecalculate = this.cards.slice(setIndex);
    setsToRecalculate.forEach(set => {
      if (this.isSetFilled(set.name) && set.search) {
        set.search();
      }
    });
  }

  hasCards() {
    return this.cards.some(c => c.set.some(s => s.rank || s.suit));
  }

  isAllFilled() {
    const setsNames = this.cards.map(c => c.name);
    return setsNames.every(name => this.isSetFilled(name));
  }

  isSetFilled(setName: string) {
    const set = this.cards.find(c => c.name === setName);
    if (!set) return;
    return set.set.every(s => s.rank && s.suit);
  }

  isRankEnabled(rank: string) {
    const cards = this.getAllCards();
    return cards.filter(c => c.rank === rank).length < 4;
  }

  isSuitEnabled(suit: string) {
    const currentCard = this.findCurrentCard();
    if (!currentCard) return;
    const rank = currentCard.rank;
    const cards = this.getAllCards();
    return !cards.filter(c => c.rank === rank).find(c => c.suit === suit);
  }

  fetchFlopStats() {
    this.websocketService.send('flopSearch', {
      pocket_cards: this.getStringifiedCards(this.cards[0].set),
      flop_cards: this.getStringifiedCards(this.cards[1].set),
    })
  }

  fetchTurnStats() {
    this.websocketService.send('turnSearch', {
      pocket_cards: this.getStringifiedCards(this.cards[0].set),
      flop_cards: this.getStringifiedCards(this.cards[1].set),
      turn_card: this.getStringifiedCards(this.cards[2].set)
    })
  }

  fetchRiverStats() {
    this.websocketService.send('riverSearch', {
      pocket_cards: this.getStringifiedCards(this.cards[0].set),
      flop_cards: this.getStringifiedCards(this.cards[1].set),
      turn_card: this.getStringifiedCards(this.cards[2].set),
      river_card: this.getStringifiedCards(this.cards[3].set)
    })
  }

  findCurrentCard() {
    const selectedSet = this.cards.find(c => c.name === this.selectedSet);
    if (!selectedSet) return;
    return selectedSet.set[this.selectedPosition];
  }

  isCardSelected(set: string, position: number) {
    return this.selectedSet === set && this.selectedPosition === position;
  }

  isRankHighlighted(rank: string) {
    const currentCard = this.findCurrentCard();
    if (!currentCard) return;
    return currentCard.rank === rank;
  }

  isRankSelectable() {
    return this.selectRank;
  }

  isSuitSelectable() {
    return !this.selectRank;
  }

  openModalForMe(stats: any, type: string) {
    this.googleAnalyticsService.addStatEvent('show_possible_hands_for_me', type, '', 'click', 1);
    this.modalStats = stats;
    this.showModal = true;
    this.modalMode = 'me';
    this.modalType = type;
  }

  openModalForOther(stats: any, type: string) {
    this.googleAnalyticsService.addStatEvent('show_possible_better_hands_for_others', type, '', 'click', 1);
    this.modalStats = stats;
    this.showModal = true;
    this.modalMode = 'other';
    this.modalType = type;
  }

  openTACModal() {
    this.googleAnalyticsService.addStatEvent('open_terms_and_conditions', '', '', 'click', 1);
    this.showTACModal = true;
  }

  onCloseModal() {
    this.showModal = false;
    if (this.modalMode === 'me') {
      this.googleAnalyticsService.addStatEvent('close_possible_hands_for_me', '', '', 'click', 1);
    } else {
      this.googleAnalyticsService.addStatEvent('close_possible_better_hands_for_others', '', '', 'click', 1);
    }
  }

  onReconnect() {
    this.reconnectTryNumber = 0;
    this.reconnectNow.next();
  }

  reset() {
    this.googleAnalyticsService.addStatEvent('reset', '', '', 'click', 1);
    this.flopStats = null;
    this.turnStats = null;
    this.riverStats = null;
    this.selectRank = true;
    this.selectedSet = this.cards[0].name;
    this.selectedPosition = 0;
    this.cards.forEach(c => {
      c.set.forEach(s => {
        s.rank = '';
        s.suit = '';
      })
    })
  }

  save() {}

  setRank(rank: string) {
    if (!this.isRankSelectable()) return;
    if (!this.isRankEnabled(rank)) return;
    this.googleAnalyticsService.addStatEvent('select_rank', '', '', 'click', 1);
    const currentCard = this.findCurrentCard();
    if (!currentCard) return;
    currentCard.rank = rank;
    currentCard.suit = '';
    this.selectRank = false;
  }

  setSuit(suit: string) {
    if (!this.isSuitSelectable()) return;
    if (!this.isSuitEnabled(suit)) return;
    this.googleAnalyticsService.addStatEvent('select_suit', '', '', 'click', 1);
    const currentCard = this.findCurrentCard();
    if (!currentCard) return;
    currentCard.suit = suit;

    this.checkStats();
    this.selectNextCard();
  }

  selectCard(set: string, position: number) {
    this.googleAnalyticsService.addStatEvent('click_on_card', '', '', 'click', 1);
    this.selectedSet = set;
    this.selectedPosition = position;
    this.selectRank = true;
  }

  selectNextCard() {
    const selectedSet = this.cards.find(c => c.name === this.selectedSet);
    if (!selectedSet) return;
    const selectedSetIndex = this.cards.indexOf(selectedSet);
    const selectedCard = selectedSet.set[this.selectedPosition];
    const selectedCardIndex = selectedSet.set.indexOf(selectedCard);

    let nextEmptyCard;
    for (let set of this.cards) {
      nextEmptyCard = set.set.find((s, i) => !s.rank && !s.suit);
      if (nextEmptyCard) {
        this.selectedSet = set.name;
        this.selectedPosition = set.set.indexOf(nextEmptyCard);
        break;
      }
    }

    if (!nextEmptyCard) {
      if (selectedSetIndex === this.cards.length - 1) {
        if (selectedCardIndex < this.cards[selectedSetIndex].set.length - 1) {
          this.selectedPosition++;
        } else {
          this.selectedSet = '';
          this.selectedPosition = 0;
        }
      } else {
        if (selectedCardIndex < this.cards[selectedSetIndex].set.length - 1) {
          this.selectedPosition++;
        } else {
          this.selectedSet = this.cards[selectedSetIndex + 1].name;
          this.selectedPosition = 0;
        }
      }
    }

    this.selectRank = true;
  }

  showCards(set: string) {
    const foundSet = this.cards.find(c => c.name === set);
    if (!foundSet) return;
    const setIndex = this.cards.indexOf(foundSet);
    if (setIndex === 0) return true;
    const prevSets = this.cards.slice(0, setIndex);
    return prevSets.every((set: any) => set.set.every(s => s.rank && s.suit));
  }

  selectFlopHand(handValue) {
    this.flopHand = handValue;
  }

  selectTurnHand(handValue) {
    this.turnHand = handValue;
  }

  selectRiverHand(handValue) {
    this.riverHand = handValue;
  }

}
