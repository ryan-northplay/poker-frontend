import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-reconnect-modal',
  templateUrl: './reconnect-modal.component.html',
  styleUrls: ['./reconnect-modal.component.scss']
})
export class ReconnectModalComponent implements OnInit {

  @Input() time: number;

  @Input() reconnecting: boolean;

  @Output() close: EventEmitter<void> = new EventEmitter<void>();

  @Output() reconnect: EventEmitter<void> = new EventEmitter<void>();

  constructor() { }

  ngOnInit(): void {
  }

  connect() {
    this.reconnect.next();
  }

  getTime() {
    return Math.round(this.time / 1000);
  }

}
