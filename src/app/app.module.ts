import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { EnterComponent } from './enter/enter.component';
import { MainComponent } from './main/main.component';
import {FormsModule} from "@angular/forms";
import {WebsocketService} from "./services/websocket.service";
import { ModalComponent } from './main/modal/modal.component';
import { GraphComponent } from './main/graph/graph.component';
import { ReconnectModalComponent } from './main/reconnect-modal/reconnect-modal.component';
import {TokenService} from "./services/token.service";
import { TermsAndConditionsModalComponent } from './main/terms-and-conditions-modal/terms-and-conditions-modal.component';
import {GoogleAnalyticsService} from "./services/googleAnalytics.service";

@NgModule({
  declarations: [
    AppComponent,
    EnterComponent,
    MainComponent,
    ModalComponent,
    GraphComponent,
    ReconnectModalComponent,
    TermsAndConditionsModalComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [WebsocketService, TokenService, GoogleAnalyticsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
